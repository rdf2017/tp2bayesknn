import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import Jama.Matrix;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import javafx.util.Pair;

public class Main extends Application{
	private int nbClusters;
	private int K;
	private ArrayList<Sample> learningSamples;
	private ArrayList<Sample> samples;
	private ArrayList<Sample> perfectSamples;
	private static final RuntimeException optionsEx = new RuntimeException("4 to 6 arguments are needed: \n  - algorithm (KNN / Bayes)\n  - test samples file\n  - samples file\n  - test classes file\n  - K : amount of neighbors (for KNN)\n  - classification of test samples (optional)\n\n");
	
	public static void main(String[] args) throws FileNotFoundException {
		if (args.length > 6 || args.length < 4) {
			throw optionsEx; 
		}
		launch(args);
	}
	
	private void computeNeighborhoodKNN() {
		for (Sample sample : samples) {	// Pour chaque sample dont on veut d�terminer le cluster
			ArrayList<Pair<Integer,Double>> clustersDistance = new ArrayList<>(learningSamples.size());
			for (Sample lSample : learningSamples) {	// Pour chaque sample d�j� classifi� on calcule le couple Cluster,Distance
				clustersDistance.add(new Pair<Integer, Double>(lSample.getCluster(), sample.distance(lSample)));
			}
			clustersDistance.sort(new Comparator<Pair<Integer, Double>>() {
				@Override
				public int compare(Pair<Integer, Double> arg0, Pair<Integer, Double> arg1) {
					if (arg0.getValue() < arg1.getValue()) return -1; else if (arg0.getValue() > arg1.getValue()) return 1; else return 0; 
				}
			});

			int[] clusterOccurrences = new int[nbClusters];
			for (int i = 0; i < K; i++) {
				clusterOccurrences[clustersDistance.get(i).getKey()]++;
			}
			int bestCluster = 0;
			for (int i = 0; i < nbClusters; i++) {
				if (clusterOccurrences[i] > clusterOccurrences[bestCluster]) 
					bestCluster = i;
			}
			sample.setCluster(bestCluster);
		}	
	}
	
	private static Cluster getAndAddClusterIfNotExists(List<Cluster> clusters, int id) {
		Cluster cluster = new Cluster(id);
		int idx = clusters.indexOf(cluster);
		if (idx == -1) {	// !contains
			clusters.add(cluster);
		} else {
			cluster = clusters.get(idx);
		}
		return cluster;
	}
	
	private void computeNeighborhoodBayes() {
		final ArrayList<Cluster> clusters = new ArrayList<Cluster>(nbClusters);
		// Learning step
		// Compute mean
		for (final Sample lSample : learningSamples) {
			final Cluster cluster = getAndAddClusterIfNotExists(clusters, lSample.getCluster()); 
			cluster.size++;
			cluster.first += lSample.getFirst();
			cluster.second += lSample.getSecond();
		}
		for (final Cluster cluster : clusters) {
			cluster.first /= cluster.size;
			cluster.second /= cluster.size;
		}
		// compute covariance
		for (final Sample lSample : learningSamples) {
			final Cluster cluster = getAndAddClusterIfNotExists(clusters, lSample.getCluster());
			final Matrix mat = new Matrix(2, 1);
			final Matrix matT = new Matrix(1, 2);
			final double x = lSample.getFirst() - cluster.first;
			final double y = lSample.getSecond() - cluster.second;
			mat.set(0, 0, x);
			mat.set(1, 0, y);
			matT.set(0, 0, x);
			matT.set(0, 1, y);
			cluster.variance.plusEquals(mat.times(matT));
		}
		for (Cluster cluster : clusters) {
			cluster.variance = cluster.variance.times(1d / (double)cluster.size);
		}
		
		// Decision step
		for (final Sample sample : samples) {
			double maxProb = Double.NEGATIVE_INFINITY;
			Cluster maxProbCluster = null;
			for (final Cluster cluster : clusters) {
				final Matrix mat = new Matrix(2, 1);
				final Matrix matT = new Matrix(1, 2);
				final double x = sample.getFirst() - cluster.first;
				final double y = sample.getSecond() - cluster.second;
				mat.set(0, 0, x);
				mat.set(1, 0, y);
				matT.set(0, 0, x);
				matT.set(0, 1, y);
				Double proba = (matT.times(-.5d).times(cluster.variance.inverse()).times(mat)).get(0, 0);
				proba -= .5d * Math.log(cluster.variance.det());
				proba += Math.log((double)cluster.size / (double)samples.size());
				if (proba > maxProb) {
					maxProb = proba;
					maxProbCluster = cluster;
				}
			}
			sample.setCluster(maxProbCluster.id);
		}
	}
	
	private double computeErrorRate() {
		int errors = 0;
		for (int i = 0; i < samples.size(); i++) {
			if (samples.get(i).getCluster() != perfectSamples.get(i).getCluster())
				errors++;
		}
		return (double)errors / (double) samples.size(); 
	}
	
	private static ArrayList<Integer> getClassesFromFile(String filePath) throws FileNotFoundException {
		final File input = new File(filePath);
		final Scanner scan = new Scanner(input);
		scan.useDelimiter(" ");
		final ArrayList<Integer> classes = new ArrayList<Integer>();
		
		while (scan.hasNext()) {
			final String toParse = scan.next();
			try {
				classes.add(new Double(Double.parseDouble(toParse) - 1d).intValue());
			} catch (NumberFormatException e) {}
		}
		scan.close();

		return classes;
	}
	

	private static ArrayList<Sample> getSamplesFromFile(String filePath) throws FileNotFoundException {
		final File input = new File(filePath);
		
		Scanner scan = new Scanner(input);
		String lineX, lineY;
		lineX = scan.nextLine();
		lineY = scan.nextLine();
		scan.close();

		scan = new Scanner(lineX);
		scan.useDelimiter(" ");
		final ArrayList<Double> doublesX = new ArrayList<Double>();
		while (scan.hasNext()) {
			final String toParse = scan.next();
			try {
				doublesX.add(Double.parseDouble(toParse));
			} catch (NumberFormatException e) {}
		}
		scan.close();
		scan = new Scanner(lineY);
		scan.useDelimiter(" ");
		final ArrayList<Double> doublesY = new ArrayList<Double>(doublesX.size());
		while (scan.hasNext()) {
			final String toParse = scan.next();
			try {
				doublesY.add(Double.parseDouble(toParse));
			} catch (NumberFormatException e) {}
		}
		scan.close();
		
		final ArrayList<Sample> samples = new ArrayList<Sample>(doublesX.size());
		for (int i = 0; i < doublesX.size(); i++) {
			samples.add(new Sample(doublesX.get(i), doublesY.get(i)));
		}
		return samples;
	}

	private void displayChart(Stage primaryStage, ArrayList<Sample> samples, String titre) {
		primaryStage.setTitle(titre);
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();        
        final ScatterChart<Number,Number> sc = new ScatterChart<Number,Number>(xAxis,yAxis);
        xAxis.setLabel("X");                
        yAxis.setLabel("Y");
        sc.setTitle(titre);
        sc.setLegendVisible(false);

        final ArrayList<XYChart.Series<Number, Number>> series = new ArrayList<XYChart.Series<Number, Number>>(nbClusters);
        for (int i = 0; i < nbClusters; i++) {
        	series.add(new XYChart.Series<Number, Number>());
        }
        for (final Sample sample : samples) {
        	series.get(sample.getCluster()).getData().add(new XYChart.Data<>(sample.getFirst(), sample.getSecond()));
        }

        sc.getData().addAll(series);
        Scene scene  = new Scene(sc, 500, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		final boolean algoIsKNN = getParameters().getRaw().get(0).equalsIgnoreCase("KNN");
		learningSamples = getSamplesFromFile(getParameters().getRaw().get(1));
		samples = getSamplesFromFile(getParameters().getRaw().get(2));
		perfectSamples = new ArrayList<Sample>(samples.size()); 
		for (Sample sample : samples) {
			perfectSamples.add((Sample)sample.clone());
		}
		final ArrayList<Integer> clusters = getClassesFromFile(getParameters().getRaw().get(3));
		for (int i = 0; i < clusters.size(); i++) {
			perfectSamples.get(i).setCluster(clusters.get(i));
		}
		
		if (getParameters().getRaw().size() == 4) {	// Bayes
			if (!algoIsKNN) {
				nbClusters = 3;
				for (int i = 0; i < learningSamples.size(); i++) {
					learningSamples.get(i).setCluster(Math.floorDiv(i, (int) ((double)learningSamples.size() / (double)nbClusters)));
				}
			} else throw optionsEx;
		} else if (getParameters().getRaw().size() == 5) {	// Bayes + orig OR KNN
			if (algoIsKNN) {
				nbClusters = 3;
				K = Integer.parseInt(getParameters().getRaw().get(4));
				for (int i = 0; i < learningSamples.size(); i++) {
					learningSamples.get(i).setCluster(Math.floorDiv(i, (int) ((double)learningSamples.size() / (double)nbClusters)));
				}
			} else {
				final ArrayList<Integer> lClusters = getClassesFromFile(getParameters().getRaw().get(4));
				final ArrayList<Cluster> uniqueClusters = new ArrayList<Cluster>();
				for (int i = 0; i < learningSamples.size(); i++) {
					learningSamples.get(i).setCluster(lClusters.get(i));
					getAndAddClusterIfNotExists(uniqueClusters, learningSamples.get(i).getCluster());
				}				
				nbClusters = uniqueClusters.size();
			}
		} else if (getParameters().getRaw().size() == 6) {	// KNN + orig
			if (algoIsKNN) {
				nbClusters = 3;
				K = Integer.parseInt(getParameters().getRaw().get(4));
				final ArrayList<Integer> lClusters = getClassesFromFile(getParameters().getRaw().get(5));
				final ArrayList<Cluster> uniqueClusters = new ArrayList<Cluster>();
				for (int i = 0; i < learningSamples.size(); i++) {
					learningSamples.get(i).setCluster(lClusters.get(i));
					getAndAddClusterIfNotExists(uniqueClusters, learningSamples.get(i).getCluster());
				}				
				nbClusters = uniqueClusters.size();
			} else throw optionsEx;
		}
		
		if (algoIsKNN) {
			if (K > learningSamples.size()) {
				throw new IllegalArgumentException("K can not be greater than the amount of learning samples !");
			}
			computeNeighborhoodKNN();
		} else {
			computeNeighborhoodBayes();
		}
			
		final Comparator<Sample> sampleSorter = new Comparator<Sample>() {
			@Override public int compare(Sample o1, Sample o2) {
				if (o1.getFirst() < o2.getFirst()) {
					if (o1.getSecond() < o2.getSecond()) {
						return -4;
					} else if (o1.getSecond() > o2.getSecond()) {
						return -2;
					} else {
						return -3;
					}
				} else if (o1.getFirst() > o2.getFirst()) {
					if (o1.getSecond() < o2.getSecond()) {
						return 2;
					} else if (o1.getSecond() > o2.getSecond()) {
						return 4;
					} else {
						return 3;
					}
				} else {
					if (o1.getSecond() < o2.getSecond()) {
						return -1;
					} else if (o1.getSecond() > o2.getSecond()) {
						return 1;
					} else {
						return 0;
					}
				}
			}
		};
		samples.sort(sampleSorter);
		perfectSamples.sort(sampleSorter);
		
		final double errorRate = computeErrorRate();
		System.out.println(errorRate == 0d ? "Les �chantillons sont tous dans le bon cluster" : "Les �chantillons ne sont pas tous dans le bon cluster");
		System.out.println("Ratio d'erreur : " + errorRate * 100 + "%");
		
		displayChart(primaryStage, samples, algoIsKNN ? "KNN results" : "Bayes results");
		displayChart(new Stage(), perfectSamples, "Perfect results");
	}
}