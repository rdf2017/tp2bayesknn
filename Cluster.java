import java.util.Collection;

import Jama.Matrix;

public class Cluster {
	public int id = -1;
	public double first = new Double(0d), second = new Double(0d);
	public Matrix variance = new Matrix(2, 2);
	public int size = 0;
	
	public Cluster(Integer id) {
		this. id = id;
	}
	
	public void computeBarycenter(Collection<Sample> samples) {
		first = 0d;
		second = 0d;
		for (Sample sample : samples) {
			first += sample.getFirst();
			second += sample.getSecond();
		}
		first /= samples.size();
		second /= samples.size();
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Cluster && id == ((Cluster)obj).id;
	}
}
