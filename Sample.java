import javafx.util.Pair;

public class Sample {	
	private static Pair<Double, Double> deviation = new Pair<>(1d, 1d);
	private double first, second; 
	private int cluster;
	
	public Sample() {
		first = 0.d; 
		second = 0.d;
		this.cluster = 0;
	}

	public Sample(Double arg0, Double arg1) {
		first = arg0;
		second = arg1;
		this.cluster = 0;
	}

	public void setCluster(int cluster) {
		this.cluster = cluster;
	}
	
	public int getCluster() {
		return this.cluster;
	}

	/**
	 * @return the first
	 */
	public double getFirst() {
		return first;
	}

	/**
	 * @param first the first to set
	 */
	public void setFirst(double first) {
		this.first = first;
	}

	/**
	 * @return the second
	 */
	public double getSecond() {
		return second;
	}

	/**
	 * @param second the second to set
	 */
	public void setSecond(double second) {
		this.second = second;
	}
	
	public double distance(Sample sample) {
		return Math.sqrt(((this.first - sample.first) * (this.first - sample.first)) / deviation.getKey() +
			((this.second - sample.second) * (this.second - sample.second)) / deviation.getValue());
	}
	
	@Override
	public Object clone() {
		final Sample sample = new Sample();
		sample.setFirst(first);
		sample.setSecond(second);
		sample.setCluster(cluster);
		return sample;
	}
}
